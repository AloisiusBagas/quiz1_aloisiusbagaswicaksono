﻿import Axios from 'axios';
import { HandphoneModelTemp } from '../models/IHandphoneModel'
import { ResponseModelTemp } from '../models/ResponseModelTemp';

export class HandphoneService {
   
    async getAllHandphone(): Promise<HandphoneModelTemp[]> {
        const response = await Axios.get<HandphoneModelTemp[]>('/api/v1/handphone/all-hanphone');
        if (response.status === 200) {
            return response.data;
        }
        return [];
    }

    async insertHandphone(data: HandphoneModelTemp): Promise<string> {
        const response = await Axios.post<ResponseModelTemp>('/api/v1/handphone/insert', data);
        if (response.status === 200) {
            return response.data.responseMessage
        }
        return response.data.responseMessage;
    }

    async updateHandphone(data: HandphoneModelTemp): Promise<string> {
        const response = await Axios.put<ResponseModelTemp>('/api/v1/handphone/update', data);
        if (response.status === 200) {
            return response.data.responseMessage;
        }
        return response.data.responseMessage;
    }

    async deleteEmployee(data: string): Promise<string> {
        const response = await Axios.delete<ResponseModelTemp>('/api/v1/handphone/delete-2/' + data);
        if (response.status === 200) {
            return response.data.responseMessage;
        }
        return response.data.responseMessage;
    }

    async getFilterData(pageIndex: number, itemPerPage: number, filterByName: string): Promise<HandphoneModelTemp[]> {
        const response = await Axios.get<HandphoneModelTemp[]>('/api/v1/handphone/filter-data?filterByName='+filterByName+'&pageIndex='+pageIndex+'&itemPerPage='+ itemPerPage);
        if (response.status === 200) {
            return response.data;
        }

        return [];
    }

    async getTotalData(): Promise<number> {
        const response = await Axios.get<number>('/api/v1/handphone/total-data');
        return response.data;
    }

}

export const HandphoneServiceSingleton = new HandphoneService();