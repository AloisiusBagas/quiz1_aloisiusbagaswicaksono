﻿export interface HandphoneModelTemp {
    // sebagai data model yg digunakan untuk axios
    handphoneID?: number;
    handphoneName?: string;
    handphoneCount?: number;
}