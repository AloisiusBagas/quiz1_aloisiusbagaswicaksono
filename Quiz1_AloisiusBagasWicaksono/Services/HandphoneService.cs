﻿using Microsoft.EntityFrameworkCore;
using Quiz1_AloisiusBagasWicaksono.Entities;
using Quiz1_AloisiusBagasWicaksono.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_AloisiusBagasWicaksono.Services
{
    public class HandphoneService
    {
        private readonly ManagementDbContext _db;
        public HandphoneService(ManagementDbContext dbContext)
        {
            this._db = dbContext;
        }

        /// <summary>
        /// Untuk menambah data handphone baru kedalam database dan table
        /// </summary>
        /// <param name="name"></param>
        /// <param name="count"></param>
        /// <returns>boolean</returns>
        public async Task<bool> InsertHandphone(string name, int count)
        {
            this._db.Add(new Handphone
            {
                HandphoneName = name,
                HandphoneCount = count,
            });
            await this._db.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Untuk mendapatkan data handphone, yang digunakan saat filtering dan pagination
        /// </summary>
        /// <param name="filtername"></param>
        /// <param name="pageindex"></param>
        /// <param name="itemperpage"></param>
        /// <returns>Filtered card</returns>
        public async Task<List<HandphoneModel>> GetAsync(string filtername, int pageindex, int itemperpage)
        {
            var Handphone_Query = this._db
            .Handphones
            .AsQueryable();

            if (string.IsNullOrEmpty(filtername) == false)
            {
                Handphone_Query = Handphone_Query
                    .Where(Q => Q.HandphoneName.StartsWith(filtername));
            }

            var handphones = await Handphone_Query
                .Select(Q => new HandphoneModel
                {
                    HandphoneID = Q.HandphoneID,
                    HandphoneName = Q.HandphoneName,
                    HandphoneCount = Q.HandphoneCount
                })
                .Skip((pageindex - 1) * itemperpage)
                .Take(itemperpage)
                .AsNoTracking()
                .ToListAsync();
            return handphones;
        }

        /// <summary>
        /// Untuk mengupdate data handphone, berdasarkan parameter id yang dikirim
        /// </summary>
        /// <param name="handphoneModel"></param>
        /// <returns>boolean</returns>
        public async Task<bool> UpdateHandphone(int id, string name, int count)
        {
            var handphone = await this._db
                .Handphones
                .Where(Q => Q.HandphoneID ==id)
                .FirstOrDefaultAsync();
            if (handphone == null)
            {
                return false;
            }
            handphone.HandphoneName = name;
            handphone.HandphoneCount = count;

            await this._db.SaveChangesAsync();
            return true;

        }

        /// <summary>
        /// Untuk mendapatkan data spesifik handphone, berdasarkan parameter id yang dikirim
        /// </summary>
        /// <param name="handphoneid"></param>
        /// <returns>Specific handphone data</returns>
        public async Task<HandphoneModel> GetSpecificHandphoneAsync(int? handphoneid)
        {
            var handphone = await this._db
                .Handphones
                .Where(Q => Q.HandphoneID == handphoneid)
                .Select(Q => new HandphoneModel
                {
                    HandphoneID = Q.HandphoneID,
                    HandphoneName = Q.HandphoneName,
                    HandphoneCount = Q.HandphoneCount
                })
                .FirstOrDefaultAsync();
            return handphone;

        }

        /// <summary>
        /// Untuk menghapus handphone, berdasarkan parameter id yang dikirim
        /// </summary>
        /// <param name="handphoneid"></param>
        /// <returns>boolean</returns>
        public async Task<bool> DeleteHandphone(int handphoneid)
        {
            var handphone = await this._db
                .Handphones
                .Where(Q => Q.HandphoneID == handphoneid)
                .FirstOrDefaultAsync();
            if (handphone == null)
            {
                return false;
            }
            this._db.Remove(handphone);
            await this._db.SaveChangesAsync();
            return true;
        }
        /// <summary>
        /// Untuk mendapatkan semua data handphone dalam bentuk list, digunakan saat view data
        /// </summary>
        /// <returns>List all of handphones</returns>
        public async Task<List<HandphoneModel>> GetAllHandphonesAsync()
        {
            var handphones = await this._db
                .Handphones
                .Select(Q => new HandphoneModel
                {
                    HandphoneName = Q.HandphoneName,
                    HandphoneCount = Q.HandphoneCount,
                    HandphoneID = Q.HandphoneID
                })
                .AsNoTracking()
                .ToListAsync();
            return handphones;
        }

        /// <summary>
        /// Untuk mendapatkan jumlah total nama handphone di database
        /// </summary>
        /// <returns>Total count handphone in datbase </returns>
        public int GetTotalData()
        {
            var totalHandphone = this._db
                .Handphones
                .Count();
            return totalHandphone;
        }


    }
}

