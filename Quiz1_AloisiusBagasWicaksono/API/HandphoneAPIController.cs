﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Quiz1_AloisiusBagasWicaksono.Model;
using Quiz1_AloisiusBagasWicaksono.Services;

namespace Quiz1_AloisiusBagasWicaksono.API
{
    [Route("api/v1/handphone")]
    [ApiController]
    public class HandphoneAPIController : ControllerBase
    {
        private readonly HandphoneService _ServiceHandphone;

        public HandphoneAPIController(HandphoneService handphoneService)
        {
            this._ServiceHandphone = handphoneService;
        }

        [HttpGet("all-hanphone", Name = "allhandphone")]
        public async Task<IActionResult> GettAllHandphonesAsync()
        {
            var handphone = await _ServiceHandphone.GetAllHandphonesAsync();
            return Ok(handphone);
        }

        [HttpPost("insert", Name = "inserthandphone")]
        public async Task<ActionResult<ResponseModel>> InsertNewHandphoneAsync([FromQuery] string name, int count)
        {
            var isSuccess = await _ServiceHandphone.InsertHandphone(name,count);
            return Ok(new ResponseModel
            {
                ResponseMessage = "Success to insert new data."
            });
        }

        [HttpPut("update", Name = "updatehandphone")]
        public async Task<ActionResult<ResponseModel>> UpdateHandphoneAsync([FromQuery] int id, string name, int count)
        {
            var isSuccess = await _ServiceHandphone.UpdateHandphone(id,name,count);
            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID Not Found!!!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success update data"
            });
        }

        [HttpDelete("delete", Name = "deletehandphone")]
        public async Task<ActionResult<ResponseModel>> DeleteEmployeeAsync([FromBody] HandphoneModel value)
        {
            var isSuccess = await _ServiceHandphone.DeleteHandphone(value.HandphoneID);
            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id not found"
                });
            }
            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success delete employee {value.HandphoneName}"
            });
        }

        [HttpGet("handphone-specific", Name = "getspecifichandphone")]
        public async Task<ActionResult<HandphoneModel>> GetSpecificHandphoneAsync(int? handphoneid)
        {
            if(handphoneid.HasValue == false)
            {
                return BadRequest(null);
            }
            var handphone = await _ServiceHandphone.GetSpecificHandphoneAsync(handphoneid.Value);
            if (handphone == null)
            {
                return BadRequest(null);
            }
            return Ok(handphone);
        }

        [HttpDelete("delete-2/{handphoneId}", Name = "deletehandphone2")]
        public async Task<ActionResult<ResponseModel>> DeleteHandphone2Async(int handphoneid)
        {
            var isSuccess = await _ServiceHandphone.DeleteHandphone(handphoneid);
            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Handphone Id not found"
                });
            }
            return Ok(new ResponseModel
            {
                ResponseMessage = "Success delete handphone"
            });
        }

        [HttpDelete("delete-query")]
        public async Task<IActionResult> DeleteHandphoneQueryAsync([FromQuery] int handphoneId)
        {
            var isSuccess = await _ServiceHandphone.DeleteHandphone(handphoneId);
            if (isSuccess == false)
            {
                return BadRequest("Id not found!");
            }

            return Ok(isSuccess);
        }

        [HttpGet("filter-data")]
        public async Task<ActionResult<List<HandphoneModel>>> GetFilterDataAsync([FromQuery] int pageIndex, int itemPerPage, string filterByName)
        {
            var data = await _ServiceHandphone.GetAsync(filterByName, pageIndex, itemPerPage);
            return Ok(data);
        }

        [HttpGet("total-data")]
        public ActionResult<int> GetTotalData()
        {
            var totaldata = _ServiceHandphone.GetTotalData();
            return Ok(totaldata);
        }
    }
}