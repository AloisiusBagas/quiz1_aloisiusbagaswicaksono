﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_AloisiusBagasWicaksono.Model
{
    public class ResponseModel
    {
        /// <summary>
        /// Untuk API saat mengirim pesan balik
        /// </summary>
        public string ResponseMessage { set; get; }
        public string Status { set; get; }
    }
}
