﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_AloisiusBagasWicaksono.Model
{
    public class HandphoneModel
    {
        /// <summary>
        /// HandphoneID adalah Primary key dan auto increment
        /// </summary>
        public int HandphoneID { get; set; }

        /// <summary>
        /// Digunakan untuk mengisi nama handphone dengan panjang karakter minimal 3 dan maksimal 7
        /// </summary>
        [Required]
        [Display(Name = "Nama Handphone")]
        [MinLength(3), MaxLength(7)]
        public string HandphoneName { get; set; }
        /// <summary>
        /// Digunakan untuk mengisi jumlah dari setiap merk/nama handphone
        /// </summary>
        [Required]
        [Display(Name = "Jumlah")]
        public int HandphoneCount { get; set; }
    }
}
