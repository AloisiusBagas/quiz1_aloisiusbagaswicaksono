﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quiz1_AloisiusBagasWicaksono.Entities
{
    public class ManagementDbContext : DbContext
    {
        public ManagementDbContext(DbContextOptions<ManagementDbContext> options) : base(options)
        {
        }
        public DbSet<Handphone> Handphones { get; set; }

        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            modelbuilder.Entity<Handphone>().ToTable("handphone");

            modelbuilder.Entity<Handphone>()
                .Property(Q => Q.HandphoneName)
                .HasDefaultValue("SYSTEM");
        }
    }
}
