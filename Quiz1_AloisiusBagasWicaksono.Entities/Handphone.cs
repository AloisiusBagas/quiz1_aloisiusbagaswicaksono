﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Quiz1_AloisiusBagasWicaksono.Entities
{
    public class Handphone
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HandphoneID { get; set; }
        [Required]
        public string HandphoneName { get; set; }
        [Required]
        public int HandphoneCount { get; set; }
    }
}
