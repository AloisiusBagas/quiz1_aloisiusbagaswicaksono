﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Quiz1_AloisiusBagasWicaksono.Entities.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "handphone",
                columns: table => new
                {
                    HandphoneID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    HandphoneName = table.Column<string>(nullable: false, defaultValue: "SYSTEM"),
                    HandphoneCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_handphone", x => x.HandphoneID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "handphone");
        }
    }
}
